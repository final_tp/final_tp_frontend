import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    students: [],
    student: {}
}

export const studentSlice = createSlice({
    name: 'student',
    initialState,
    reducers: {
        addStudent: (state, action) => {
            state.students.push(action.payload)
        },
        getStudent: (state, action) => {
            state.students = action.payload.map(stu =>{
                return{
                    _id: stu._id,
                    name_student: stu.name_student,
                    description_student: stu.description_student,
                    date_enter: stu.date_enter
                }
            })
        },
        updateStudent: (state, action) => {
            const index = state.students.findIndex(a => a.id === action.payload.id)
            // const index = state.students.findIndex(b => b.date_enter === action.payload.date_enter)
            state.students[index] = {
                _id: action.payload._id,
                name_student: action.payload.name_student,
                description_student: action.payload.description_student,
                date_enter: action.payload.date_enter
            }
        },
        deleteStudent: (state, action) => {
            const id = action.payload;

            const date_enter = action.payload.date_enter
            state.students = state.students.filter(a => a._id !== id)
        }
    },
})

// Action creators are generated for each case reducer function
export const { addStudent, getStudent, updateStudent, deleteStudent } = studentSlice.actions

export default studentSlice.reducer
