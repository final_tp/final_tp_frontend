import ListGroup from 'react-bootstrap/ListGroup';
import Accordion from 'react-bootstrap/Accordion';
import { useSelector } from 'react-redux'
import Button from 'react-bootstrap/Button';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { deleteStudent, updateStudent, getStudent } from '../../../Features/Students/studentSlice';
import './ListStudents.css'
import axios from 'axios'
import { Link } from 'react-router-dom';

const ListStudents = () => {
    const chooseStyle = ["secondary", "danger", , "success", "warning", "info", "light", "dark"]
    const randomChooseStyle = Math.floor(Math.random() * chooseStyle.length);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const students = useSelector(state => state.student.students)
    console.log(students)

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get("http://127.0.0.1:5000/finalProject/students", {
                    headers: {
                        "Content-Type": "application/json"
                    }
                })
                dispatch(getStudent(response.data))
            } catch (err) {
                console.log(err)
            }
        }

        fetchData()
    }, [])

    const handleDelete = (e, main_id) => {
        e.preventDefault();
        axios.delete("http://127.0.0.1:5000/finalProject/students/" + main_id)
            .then(res => {
                dispatch(deleteStudent(main_id))
                console.log(res)
                //    navigate('/')
            })
        // dispatch(deleteStudent(main_id)) 
        // navigate('/')
    }

    return (
        <>
            <div className="container mt-3 mb-3">
                <h1 className="text-center">List of students</h1>
            </div>

            {/* <ListGroup className="container">
                {students && students.length > 0 && students.map((student, key) => (
                    <ListGroup.Item action variant={chooseStyle[randomChooseStyle]} key={key} className="mt-1">
                        <div className="student--flex">
                            <div className="student--flex--nom">
                                {student.name_student}
                            </div>


                            <div>
                                <Button variant="success" onClick={() => navigate(`/update-student/${student._id}`)}>Modifier</Button>
                                <Button variant="danger" onClick={(e) => handleDelete(e, student._id)}>Supprimer</Button>
                            </div>
                        </div>
                    </ListGroup.Item>
                ))}
            </ListGroup> */}


            <Accordion className="container" defaultActiveKey="0">
                {students && students.length > 0 && students.map((student, key) => (
                    <Accordion.Item key={key} eventKey={key}>
                        <Accordion.Header>{student.name_student}</Accordion.Header>
                        <Accordion.Body>
                            <h5 className='mb-3'>Description of the student</h5>
                            <div className='mb-3'>
                                {student.name_student}
                            </div>

                            <div>
                                {/* <Link className='success' to={`/update-student/${student.id}`}>Modifier</Link> */}
                                <Button variant="success" onClick={() => navigate(`/update-student/${student._id}`)}>Modifier</Button>
                                <Button variant="danger" onClick={(e) => handleDelete(e, student._id)}>Supprimer</Button>
                            </div>
                        </Accordion.Body>
                    </Accordion.Item>
                ))}
            </Accordion>


        </>
    )
}

export default ListStudents;
