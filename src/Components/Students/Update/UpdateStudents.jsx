import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { useParams } from 'react-router-dom';
import { UseSelector } from 'react-redux';
import { updateStudent } from '../../../Features/Students/studentSlice';
import axios from 'axios';

function UpdateStudent() {
    const { id } = useParams();
    const students = useSelector( state => state.student.students )    
    const stu = students.find(u => u._id === id)
    console.log(stu)
    console.log("Id De la page",id)
    const [student, setStudent] = useState({
        _id : stu._id,
        name_student: stu.name_student,
        description_student: stu.description_student,
        date_enter: Date.now()
    })
    console.log(students)

    const dispatch = useDispatch();
    const navigate = useNavigate();


    const handleUpdate = (e) => {
        e.preventDefault();
        console.log(student)
        // setStudent({...student, date_enter: Date.now()})
        // setStudent({...student, _id: id})

        axios.put(`http://127.0.0.1:5000/finalProject/students/${id}`, student)
          .then(response => {
            console.log(response)
            dispatch(updateStudent(student))
            navigate("/list-students")
        })
          .catch(err => {console.log(err)});
    }

    return (
        <>
        <div className='container'>
            <h3 className='"text-center'>Update Page</h3>
        </div>
            <Form className='container'>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Name of the student</Form.Label>
                    <Form.Control type="text" placeholder="Enter the name of the student" value={student.name_student} onChange={(e) => (setStudent({ ...student, name_student: e.target.value }))} />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Description of the student</Form.Label>
                    <Form.Control as='textarea' rows={3} placeholder="Enter the description of student" value={student.description_student} onChange={(e) => (setStudent({ ...student, description_student: e.target.value }))} />
                </Form.Group>

                <Button variant="primary" type="submit" onClick={handleUpdate}>
                    Submit
                </Button>
            </Form>
        </>
    );
}

export default UpdateStudent;
