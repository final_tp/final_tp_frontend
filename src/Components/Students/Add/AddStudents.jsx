import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { addStudent } from '../../../Features/Students/studentSlice';
import axios from 'axios'

function AddStudents() {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [student, setStudent] = useState({
        name_student: "",
        description_student: "",
        date_enter: Date.now(),
    })

    const handleSumbit = (e) => {
        e.preventDefault();
        console.log(student)
        axios.post("http://127.0.0.1:5000/finalProject/students", student)
          .then(response => {
            console.log(response)
           dispatch(addStudent(response.data))
            navigate("/list-students")
          })
         .catch(err => {console.log(err)});
        
        //  dispatch(addStudent(student))
        //  navigate("/list-students")
    }

  return (
    <Form className='container'>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Name of the student</Form.Label>
        <Form.Control type="text" placeholder="Enter the name of the student" value={student.name_student} onChange={(e) => (setStudent({ ...student, name_student: e.target.value }))} />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Description of the student</Form.Label>
        <Form.Control as='textarea' rows={3} placeholder="Enter the description of student" value={student.description_student} onChange={(e) => (setStudent({ ...student, description_student: e.target.value }))} />
      </Form.Group>

      <Button variant="primary" type="submit" onClick={handleSumbit}>
        Submit
      </Button>
    </Form>
  );
}

export default AddStudents;
