import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link } from 'react-router-dom';

function Linking() {
    return (
        <Navbar expand="lg" className="bg-body-tertiary">
            <Container>
                <Navbar.Brand href="#home">MERN Project Students</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <NavDropdown title="Students" id="basic-nav-dropdown">
                            <Link to='/'>
                                <NavDropdown.Item href="/">
                                    Ajouter
                                </NavDropdown.Item>
                            </Link>
                            <NavDropdown.Divider />
                            <Link to='/list-students'>
                                <NavDropdown.Item href="/list-students">
                                    Lister
                                </NavDropdown.Item>
                            </Link>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Linking;