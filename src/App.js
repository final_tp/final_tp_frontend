import logo from './logo.svg';
import './App.css';
import { Route, Routes } from 'react-router-dom';
import Linking from './Components/Linking/Linking';
import { Error } from './Components/Error/Error';
import AddStudents from './Components/Students/Add/AddStudents';
import ListStudents from './Components/Students/List/ListStudents';
import UpdateStudents from './Components/Students/Update/UpdateStudents';
import axios from 'axios';
import { getStudent } from './Features/Students/studentSlice';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

function App() {
  const dispatch = useDispatch()

  return (
    
    <div className="App">
      <Linking />

      <Routes>
        <Route path="/" element={<AddStudents />} />
        <Route path="/list-students" element={<ListStudents />} />
        <Route path="/update-student/:id" element={<UpdateStudents />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </div>
  );
}

export default App;
